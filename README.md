# API Setup Instructions

## Initial Requirements
* Node: v14.17.0
* Npm: 7.7.5
* MongoDB: 4.4

Please make sure, you have all of the above installed.

## Steps for Code Setup
1. npm install
2. npm start

## API Documentation
### [http://localhost:3030/docs](http:localhost:3030/docs)

## Details
* 3 Layer Architecture:
    * Controller (Express Router Controller)
    * Service Layer (Service Class. Implements Business Logics)
    * Data Access Layer (Mongoose ODM)
* Authentication: Passwordless Login
* Mailer API: Mandrill
* Logger: Winston
* API Validation: Celebrate
* Data Modeling: Mongoose
* Subscription to events
* Dependency Injection
