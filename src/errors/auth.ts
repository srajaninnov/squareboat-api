export default {
    COULD_NOT_SEND_OTP: {
        mesage: 'Could not send OTP'
    },
    INVOKE_SEND_OTP_FIRST: {
        message: 'Invoke send OTP first'
    },
    INVALID_OTP: {
        message: 'Invalid OTP'
    }
}