export default {
    ERROR_CREATING_CART_ITEM : {
        mesage: 'Error Creating Cart Item'
    },
    ERROR_FINDING_CART_ITEM: {
        message: 'Error Finding Cart Item'
    },
    ERROR_FINDING_CART_ITEM_BY_ID: {
        message: 'Error Finding Cart Item By Id'
    },
    ERROR_UPDATING_CART_ITEM: {
        message: 'Error Updating Cart Item'
    },
    ERROR_DELETING_CART_ITEM: {
        message: 'Error Deleting Cart Item'
    },
    ERROR_DELETING_CART_ITEMS_AFTER_ORDER_CONFIRMATION: {
        message: 'Error Deleting Cart Items after Order Confirmation'
    },
    DUPLICATE_CART_ITEM_EXISTS: {
        message: 'Duplicate Cart Item exists'
    },
    CART_ITEM_CANNOT_BE_CREATED: {
        message: 'Cart Item cannot be created'
    },
    CART_ITEM_NOT_FOUND: {
        message: 'Cart Item not found'
    },
    CART_ITEM_COULD_NOT_BE_UPDATED: {
        message: 'Cart Item could not be updated'
    },
    CART_ITEM_CANNOT_BE_DELETED: {
        message: 'Cart Item cannot be deleted'
    },
    CART_ITEMS_BY_USER_ID_NOT_FOUND: {
        message: 'Cart Items by User Id not found'
    }
}