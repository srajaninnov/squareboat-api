export default {
    ERROR_CREATING_ORDER : {
        mesage: 'Error Creating Order'
    },
    ERROR_FINDING_ORDER: {
        message: 'Error Finding Order'
    },
    ERROR_FINDING_ORDER_BY_ID: {
        message: 'Error Finding Order By Id'
    },
    ERROR_UPDATING_ORDER: {
        message: 'Error Updating Order Item'
    },
    ERROR_DELETING_ORDER: {
        message: 'Error Deleting Order Item'
    },
    ORDER_CANNOT_BE_CREATED: {
        message: 'Order cannot be created'
    },
    CART_COULD_NOT_BE_DELETED_AFTER_ORDER_CONFIRMATION: {
        message: 'Cart could not deleted after Order confirmation'
    },
    ORDER_NOT_FOUND: {
        message: 'Order not found'
    },
    ORDERS_NOT_FOUND_FOR_THIS_USER: {
        message: 'Orders not found for this user'
    },
    ORDER_COULD_NOT_BE_UPATED: {
        message: 'Order could not be updated'
    },
    ORDER_CANNOT_BE_DELETED: {
        message: 'Order cannot be deleted'
    }

}