import { Service, Inject } from 'typedi';
import { IUser } from '../db/interfaces/IUser';

@Service()
export default class MailerService {
  constructor(
    @Inject('emailClient') private emailClient
  ) { }


  OTPMailTemplate = (otp) => {
    return `<div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2">
    <div style="margin:50px auto;width:70%;padding:20px 0">
      <h2 style="background: #2D94F3;margin: 0 auto;width: max-content;padding: 0 10px;color: #fff;border-radius: 4px;">${otp}</h2>
    </div>
  </div>`
  }
  
  
  public async SendOTPEmail(otpObj) {
    /**
     * Calling Mailchimp to send OTP
     */
    console.log(otpObj);
    
    const message = {
      from_email: 'no-reply@getboarded.com',
      to: [{
        email: otpObj.email,
        type: 'to'
      }],
      subject: 'Verification OTP',
      text: otpObj.otp,
      html: this.OTPMailTemplate(otpObj.otp)
    };

    try {
      const mailResponse = await this.emailClient.messages.send({message});
      return {mailResponse, delivered: 1, status: 'ok' };
    } catch(e) {
      throw new Error(e);
      // return  {error: e, delivered: 0, status: 'error' };
    }
  }

  
  public async SendWelcomeEmail(email) {
    /**
     * @TODO Call Mailchimp/Sendgrid or whatever
     */
    // Added example for sending mail from mailgun
    const data = {
      from: 'Excited User <me@samples.mailgun.org>',
      to: [email],
      subject: 'Hello',
      text: 'Testing some Mailgun awesomness!'
    };
    try {
      const mailResponse = await this.emailClient.messages.send(data);
      return { delivered: 1, status: 'ok' };
    } catch(e) {
      return  { delivered: 0, status: 'error' };
    }
  }


  public StartEmailSequence(sequence: string, user: Partial<IUser>) {
    if (!user.email) {
      throw new Error('No email provided');
    }
    // @TODO Add example of an email sequence implementation
    // Something like
    // 1 - Send first email of the sequence
    // 2 - Save the step of the sequence in database
    // 3 - Schedule job for second email in 1-3 days or whatever
    // Every sequence can have its own behavior so maybe
    // the pattern Chain of Responsibility can help here.
    return { delivered: 1, status: 'ok' };
  }
}
