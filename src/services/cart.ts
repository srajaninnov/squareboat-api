import { Service, Inject, Container } from 'typedi';
import mongoose from 'mongoose';
// import MailerService from './mailer';
// import config from '../config';
import { ICart } from '../db/interfaces/ICart';
// import { EventDispatcher, EventDispatcherInterface } from '../decorators/eventDispatcher';
// import events from '../subscribers/events';
import CartController from '../db/controllers/cart.controller';
import Errors from '../errors/cart';

@Service()
export default class CartService {
    constructor(
        // private mailer: MailerService,
        @Inject('logger') private logger,
        // @EventDispatcher() private eventDispatcher: EventDispatcherInterface
    ) { }


    /**
     * @description Create new Cart Item
     */
    public async CreateCartItem(cartItemInput: ICart): Promise<{ data: ICart }> {
        try {
            this.logger.silly('Checking for duplicate Cart Item in db');
            let cartControllerInstance = Container.get(CartController);
            let condition = {
                _userId: mongoose.Types.ObjectId(cartItemInput._userId),
                _productId: cartItemInput._productId
            };
            let cartItemRecord = await cartControllerInstance.findRecord(condition);

            if (cartItemRecord && cartItemRecord.length) {
                throw new Error(Errors.DUPLICATE_CART_ITEM_EXISTS.message);
            }

            this.logger.silly('Creating Cart Item db record');
            cartControllerInstance = Container.get(CartController);
            cartItemRecord = await cartControllerInstance.createRecord(cartItemInput);

            if (!cartItemRecord) {
                throw new Error(Errors.CART_ITEM_CANNOT_BE_CREATED.message);
            }

            // this.logger.silly('Sending group create email');
            // await this.mailer.SendWelcomeEmail(userRecord);

            // this.eventDispatcher.dispatch(events.user.signUp, { user: userRecord });


            const data = cartItemRecord //.toObject();

            return { data };
        } catch (e) {
            this.logger.error(e);
            throw e;
        }
    }


    /**
     * 
     * @description Get Cart Item by Id
     * @async true
     */
    public async GetCartItemById(_cartItemId: string): Promise<{ data: ICart }> {
        try {
            this.logger.silly('Get Cart Item by ID from db');
            const cartControllerInstance = Container.get(CartController);
            const cartItemRecord: ICart = await cartControllerInstance.findRecordById(_cartItemId);

            if (!cartItemRecord) {
                throw new Error(Errors.CART_ITEM_NOT_FOUND.message);
            }

            let data = cartItemRecord;      

            return { data };
        } catch (e) {
            this.logger.error(e);
            throw e;
        }
    }

    /**
     * @description Get Cart Items by User Id
     * @async true
     * 
     */
    public async GetCartItemsByUserId(_userId: string): Promise<{ data: ICart }> {
        try {

            this.logger.silly('Get all Cart Items by User ID from db');
            const cartControllerInstance = Container.get(CartController);
            const cartRecord: ICart = await cartControllerInstance.findRecord({ _userId });

            if (!cartRecord) {
                throw new Error(Errors.CART_ITEMS_BY_USER_ID_NOT_FOUND.message);
            }

            let data = cartRecord;
            return { data };
        } catch (e) {
            this.logger.error(e);
            throw e;
        }
    }


    /**
     * @description Update Cart Item by Id
     */
    public async UpdateCartItemById(_cartItemId: string, cartItemInput: ICart): Promise<{ data: ICart }> {
        try {

            this.logger.silly('Updating Cart Item db record');
            const cartControllerInstance = Container.get(CartController);
            const cartItemRecord: ICart = await cartControllerInstance.updateRecord(_cartItemId, cartItemInput);

            if (!cartItemRecord) {
                throw new Error(Errors.CART_ITEM_COULD_NOT_BE_UPDATED.message);
            }

            // this.eventDispatcher.dispatch(events.user.signUp, { user: userRecord });

            const data = cartItemRecord;
            return { data };
        } catch (e) {
            this.logger.error(e);
            throw e;
        }
    }


    /**
     * @description Delete Cart Item by Id
     */
    public async DeleteCartItemById(_cartItemId: string): Promise<{ data: ICart }> {
        try {

            this.logger.silly('Deleting Cart Item by ID from database');
            const cartControllerInstance = Container.get(CartController);
            const cartItemRecord: ICart = await cartControllerInstance.deleteRecordById(_cartItemId);

            if (!cartItemRecord) {
                throw new Error(Errors.CART_ITEM_CANNOT_BE_DELETED.message);
            }

            // this.eventDispatcher.dispatch(events.user.signUp, { user: userRecord });

            const data = cartItemRecord;
            return { data };
        } catch (e) {
            this.logger.error(e);
            throw e;
        }
    }
}
