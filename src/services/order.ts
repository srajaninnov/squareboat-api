import { Service, Inject, Container } from 'typedi';
// import MailerService from './mailer';
// import config from '../config';
import { IOrder } from '../db/interfaces/IOrder';
// import { EventDispatcher, EventDispatcherInterface } from '../decorators/eventDispatcher';
// import events from '../subscribers/events';
import OrderController from '../db/controllers/order.controller';
import CartController from '../db/controllers/cart.controller';
import Errors from '../errors/order';

@Service()
export default class OrderService {
    constructor(
        // private mailer: MailerService,
        @Inject('logger') private logger,
        // @EventDispatcher() private eventDispatcher: EventDispatcherInterface
    ) { }


    /**
     * @description Create new Order
     */
    public async CreateOrder(orderInput: IOrder): Promise<{ data: IOrder }> {
        try {
            // Dummy Data
            orderInput.paymentStatus = 'success';
            orderInput.shippedDate = new Date();
            orderInput.deliveryDate = new Date();
            orderInput.deliveryDate.setDate(orderInput.shippedDate.getDate()+4); // Set for after 4 days of shipping

            this.logger.silly('Creating Order db record');
            const orderControllerInstance = Container.get(OrderController);
            const orderRecord: IOrder = await orderControllerInstance.createRecord(orderInput);

            if (!orderRecord) {
                throw new Error(Errors.ORDER_CANNOT_BE_CREATED.message);
            }

            const cartControllerInstance = Container.get(CartController);
            const cartRecord: ICart = await cartControllerInstance.deleteAllRecordsInCart({ _userId: orderInput._userId });

            if(!cartRecord) {
                throw new Error(Errors.CART_COULD_NOT_BE_DELETED_AFTER_ORDER_CONFIRMATION.message);
            }

            // this.logger.silly('Sending group create email');
            // await this.mailer.SendWelcomeEmail(userRecord);

            // this.eventDispatcher.dispatch(events.user.signUp, { user: userRecord });


            const data = orderRecord //.toObject();

            return { data };
        } catch (e) {
            this.logger.error(e);
            throw e;
        }
    }


    /**
     * 
     * @description Get Order by Id
     * @async true
     */
    public async GetOrderById(_orderId: string): Promise<{ data: IOrder }> {
        try {
            this.logger.silly('Get Order by ID from db');
            const orderControllerInstance = Container.get(OrderController);
            const orderRecord: IOrder = await orderControllerInstance.findRecordById(_orderId);

            if (!orderRecord) {
                throw new Error(Errors.ORDER_NOT_FOUND.message);
            }

            let data = orderRecord;      

            return { data };
        } catch (e) {
            this.logger.error(e);
            throw e;
        }
    }


    public async GetAllOrdersByUserId(_userId: string): Promise<{ data: IOrder }> {
        try {

            this.logger.silly('Get all Orders by User ID from db');
            const orderControllerInstance = Container.get(OrderController);
            const orderRecord: IOrder = await orderControllerInstance.findRecord({ _userId });

            if(!orderRecord) {
                throw new Error(Errors.ORDERS_NOT_FOUND_FOR_THIS_USER.message);
            }

            let data = orderRecord;
            return { data };
        } catch (e) {
            this.logger.error(e);
            throw e;
        }
    }


    /**
     * @description Update Order by Id
     */
    public async UpdateOrderById(_orderId: string, orderInput: IOrder): Promise<{ data: IOrder }> {
        try {

            this.logger.silly('Updating Order db record');
            const orderControllerInstance = Container.get(OrderController);
            const orderRecord: IOrder = await orderControllerInstance.updateRecord(_orderId, orderInput);

            if (!orderRecord) {
                throw new Error(Errors.ORDER_COULD_NOT_BE_UPATED.message);
            }

            // this.eventDispatcher.dispatch(events.user.signUp, { user: userRecord });

            const data = orderRecord;
            return { data };
        } catch (e) {
            this.logger.error(e);
            throw e;
        }
    }


    /**
     * @description Delete Order by Id
     */
    public async DeleteOrderById(_orderId: string): Promise<{ data: IOrder }> {
        try {

            this.logger.silly('Deleting Order by ID from database');
            const orderControllerInstance = Container.get(OrderController);
            const orderRecord: IOrder = await orderControllerInstance.deleteRecordById(_orderId);

            if (!orderRecord) {
                throw new Error(Errors.ORDER_CANNOT_BE_DELETED.message);
            }

            // this.eventDispatcher.dispatch(events.user.signUp, { user: userRecord });

            const data = orderRecord;
            return { data };
        } catch (e) {
            this.logger.error(e);
            throw e;
        }
    }
}
