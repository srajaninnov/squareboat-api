import defaultConfig from './default';
import stagingConfig from './staging';
import productionConfig from './production';

let config = defaultConfig;

switch(process.env.NODE_ENV) {
  case "development":
    config = defaultConfig;
    break;
  case "staging":
    config = stagingConfig;
    break;
  case "production":
    config = productionConfig;
    break;
  default:
    config = defaultConfig;
}

export default config;