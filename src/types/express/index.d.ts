import { Document, Model } from 'mongoose';
import { IUser } from '../../db/interfaces/IUser';
import { ICart } from '../../db/interfaces/ICart';
import { IOrder } from '../../db/interfaces/IOrder';

declare global {
  namespace Express {
    export interface Request {
      currentUser: IUser & Document;
    }    
  }

  namespace Models {
    export type UserModel = Model<IUser & Document>;
    export type CartModel = Model<ICart & Document>;
    export type OrderModel = Model<IOrder & Document>;
  }
}
