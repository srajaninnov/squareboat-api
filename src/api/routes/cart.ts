import { Router, Request, Response, NextFunction } from 'express';
import { Container } from 'typedi';
import CartService from '../../services/cart';
import { ICart } from '../../db/interfaces/ICart';
import middlewares from '../middlewares';
import { celebrate, Joi } from 'celebrate';
import { error, Logger } from 'winston';

const route = Router();

export default (app: Router) => {
  app.use('/carts', route);

  /**
   * @description Get cart item by Id
   */

  route.get(
    '/:_id',
    middlewares.isAuth,
    celebrate({
      body: Joi.object({}),
    }),
    async (req: Request, res: Response, next: NextFunction) => {
      const logger:Logger = Container.get('logger');
      logger.debug('Calling Get Cart Item by Id endpoint with params: %o', req.params);
      try {
        const { _id } = req.params;
        const cartServiceInstance = Container.get(CartService);
        const { data } = await cartServiceInstance.GetCartItemById(_id);
        return res.json({data}).status(200);
      } catch (e) {
        logger.error('error: %o',  e );
        return next(e);
      }
    },
  );


  /**
   * @description Get all cart items created by a user
   */

   route.get(
    '/users/:_userId',
    middlewares.isAuth,
    celebrate({
      body: Joi.object({}),
    }),
    async (req: Request, res: Response, next: NextFunction) => {
      const logger:Logger = Container.get('logger');
      logger.debug('Calling Get all Cart Items created by a user endpoint with params: %o', req.params);
      try {
        const { _userId } = req.params;
        const cartServiceInstance = Container.get(CartService);
        const { data } = await cartServiceInstance.GetCartItemsByUserId(_userId);
        return res.json({data}).status(200);
      } catch (e) {
        logger.error('error: %o',  e );
        return next(e);
      }
    },
  );


  /**
   * @description Create new Cart Item
   */

  route.post(
    '/',
    middlewares.isAuth,
    celebrate({
      body: Joi.object({
        _userId: Joi.string().required(),
        _productId: Joi.number().required(),
        quantity: Joi.number().required()
      }),
    }),
    async (req: Request, res: Response, next: NextFunction) => {
      const logger:Logger = Container.get('logger');
      logger.debug('Calling Create Cart Item endpoint with body: %o', req.body );
      try {
        const cartServiceInstance = Container.get(CartService);
        const { data } = await cartServiceInstance.CreateCartItem(req.body as ICart);
        return res.status(201).json({ data });
      } catch (e) {
        logger.error('error: %o', e);
        return next(e);
      }
    },
  );


  /**
   * @description Update Cart Item by Id
   */

  route.patch(
    '/:_id',
    middlewares.isAuth,
    celebrate({
      body: Joi.object({
        _userId: Joi.string(),
        _productId: Joi.number(),
        quantity: Joi.number()
      }),
    }),
    async (req: Request, res: Response, next: NextFunction) => {
      const logger:Logger = Container.get('logger');
      logger.debug('Calling Update Cart Item endpoint with body: %o', req.body);
      try {
        const { _id } = req.params;
        const cartServiceInstance = Container.get(CartService);
        const { data } = await cartServiceInstance.UpdateCartItemById(_id, req.body as ICart);
        return res.json({ data }).status(200);
      } catch (e) {
        logger.error('error: %o',  e );
        return next(e);
      }
    },
  );


  /**
   * @description Delete Cart Item by Id
   */

  route.delete(
    '/:_id',
    middlewares.isAuth,
    celebrate({
      body: Joi.object({}), // Empty so that user should not send anything in body
    }),
    async (req: Request, res: Response, next: NextFunction) => {
      const logger:Logger = Container.get('logger');
      logger.debug('Calling Delete Cart Item endpoint with body: %o', req.body);
      try {
        const { _id } = req.params;
        const cartServiceInstance = Container.get(CartService);
        const { data } = await cartServiceInstance.DeleteCartItemById(_id);
        return res.json({ data }).status(200);
      } catch (e) {
        logger.error('error: %o',  e );
        return next(e);
      }
    },
  );

};
