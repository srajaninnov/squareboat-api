import { Router, Request, Response, NextFunction } from 'express';
import { Container } from 'typedi';
import OrderService from '../../services/order';
import { IOrder } from '../../db/interfaces/IOrder';
import middlewares from '../middlewares';
import { celebrate, Joi } from 'celebrate';
import { error, Logger } from 'winston';

const route = Router();

export default (app: Router) => {
  app.use('/orders', route);

  /**
   * @description Get Order by Id
   */

  route.get(
    '/:_id',
    middlewares.isAuth,
    celebrate({
      body: Joi.object({}),
    }),
    async (req: Request, res: Response, next: NextFunction) => {
      const logger:Logger = Container.get('logger');
      logger.debug('Calling Get Order Item by Id endpoint with params: %o', req.params);
      try {
        const { _id } = req.params;
        const orderServiceInstance = Container.get(OrderService);
        const { data } = await orderServiceInstance.GetOrderById(_id);
        return res.json({data}).status(200);
      } catch (e) {
        logger.error('error: %o',  e );
        return next(e);
      }
    },
  );


  /**
   * @description Get all orders created by a user
   */

   route.get(
    '/users/:_userId',
    middlewares.isAuth,
    celebrate({
      body: Joi.object({}),
    }),
    async (req: Request, res: Response, next: NextFunction) => {
      const logger:Logger = Container.get('logger');
      logger.debug('Calling Get all Orders created by a user endpoint with body: %o', req.body);
      try {
        const { _adminId } = req.params;
        const orderServiceInstance = Container.get(OrderService);
        const { data } = await orderServiceInstance.GetAllOrdersByUserId(_adminId);
        return res.json({data}).status(200);
      } catch (e) {
        logger.error('error: %o',  e );
        return next(e);
      }
    },
  );


  /**
   * @description Create new Order
   */

  route.post(
    '/',
    middlewares.isAuth,
    celebrate({
      body: Joi.object({
        _userId: Joi.string().required(),
        items: Joi.array().required()
      }),
    }),
    async (req: Request, res: Response, next: NextFunction) => {
      const logger:Logger = Container.get('logger');
      logger.debug('Calling Create Order endpoint with body: %o', req.body );
      try {
        const orderServiceInstance = Container.get(OrderService);
        const { data } = await orderServiceInstance.CreateOrder(req.body as IOrder);
        return res.status(201).json({ data });
      } catch (e) {
        logger.error('error: %o', e);
        return next(e);
      }
    },
  );


  /**
   * @description Update Order Item by Id
   */

  route.patch(
    '/:_id',
    middlewares.isAuth,
    celebrate({
      body: Joi.object({
        _userId: Joi.string(),
        quantity: Joi.number(),
        shippedDate: Joi.date(),
        deliveryDate: Joi.date()
      }),
    }),
    async (req: Request, res: Response, next: NextFunction) => {
      const logger:Logger = Container.get('logger');
      logger.debug('Calling Update Order endpoint with body: %o', req.body);
      try {
        const { _id } = req.params;
        const orderServiceInstance = Container.get(OrderService);
        const { data } = await orderServiceInstance.UpdateOrderById(_id, req.body as IOrder);
        return res.json({ data }).status(200);
      } catch (e) {
        logger.error('error: %o',  e );
        return next(e);
      }
    },
  );


  /**
   * @description Delete Order by Id
   */

  route.delete(
    '/:_id',
    middlewares.isAuth,
    celebrate({
      body: Joi.object({}), // Empty so that user should not send anything in body
    }),
    async (req: Request, res: Response, next: NextFunction) => {
      const logger:Logger = Container.get('logger');
      logger.debug('Calling Delete Order endpoint with body: %o', req.body);
      try {
        const { _id } = req.params;
        const orderServiceInstance = Container.get(OrderService);
        const { data } = await orderServiceInstance.DeleteOrderById(_id);
        return res.json({ data }).status(200);
      } catch (e) {
        logger.error('error: %o',  e );
        return next(e);
      }
    },
  );

};
