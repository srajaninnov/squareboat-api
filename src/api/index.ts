import { Router } from 'express';
import auth from './routes/auth';
import user from './routes/user';
import cart from './routes/cart';
import order from './routes/order';

// guaranteed to get dependencies
export default () => {
	const app = Router();
	auth(app);
	user(app);
	cart(app);
	order(app);

	return app
}