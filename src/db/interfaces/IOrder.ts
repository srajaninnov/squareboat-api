export interface IOrder {
    _id: string;
    _userId: string;
    items: Array<Object>;
    paymentStatus: string;
    shippedDate: Date;
    deliveryDate: Date;
}