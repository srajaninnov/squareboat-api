export interface ICart {
    _id: string;
    _userId: string;
    _productId: string;
    quantity: number;
}