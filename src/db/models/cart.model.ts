import { ICart } from '../interfaces/ICart';
import mongoose, { Schema } from 'mongoose';

const Cart = new mongoose.Schema(
  {
    _userId: { 
      type: Schema.Types.ObjectId,
      required: [true, 'Required User ID']
    },

    _productId: { 
        type: Number,
        required: [true, 'Required Product ID']
    },

    quantity: { 
        type: String,
        required: [true, 'Required Quantity']
    }
  },
  { timestamps: true },
);

export default mongoose.model<ICart & mongoose.Document>('Cart', Cart);
