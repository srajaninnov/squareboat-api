import { IOrder } from '../interfaces/IOrder';
import mongoose, { Schema } from 'mongoose';

const Order = new mongoose.Schema(
  {
    _userId: { 
      type: Schema.Types.ObjectId,
      required: [true, 'Required User ID']
    },

    items: {
      type: Array,
      required: [true, 'Required Items List']
    },

    paymentStatus: { 
        type: String,
        required: [true, 'Required PaymentStatus']
    },

    shippedDate: { 
        type: Date,
        required: [true, 'Required Shipped Date']
    },

    deliveryDate: { 
        type: Date,
        required: [true, 'Required Delivery Date']
    },
  },
  { timestamps: true },
);

export default mongoose.model<IOrder & mongoose.Document>('Order', Order);
