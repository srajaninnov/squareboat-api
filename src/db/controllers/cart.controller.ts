import { Service, Inject } from "typedi";
import cartErrors from "../../errors/cart";

@Service()
export default class CartController {
    constructor(
        @Inject('cartModel') private cartModel: Models.CartModel,
        @Inject('logger') private logger,
    ) {}

    /**
     * @description This method will help create a Cart record
     * 
     * @param {Object} dataObject
     */
    async createRecord(dataObject) {
        try {
            const cartRecord = await this.cartModel.create(dataObject);
            return cartRecord
        } catch(error) {
            throw { error, message: cartErrors.ERROR_CREATING_CART_ITEM.mesage };
        }
    }

    /**
     * @description This method will help find an Cart record
     * 
     * @param {Object} condition
     */
    async findRecord(condition) {
        try {
            const cartRecord = await this.cartModel.find(condition);
            return cartRecord;
        } catch(error) {
            throw { error, message: cartErrors.ERROR_FINDING_CART_ITEM.message };
        }
    }

    /**
     * @description This method will help find a Cart Record By Id
     * 
     * @param {Object} condition
     */
     async findRecordById(_cartItemId) {
        try {
            const cartRecord = await this.cartModel.findById(_cartItemId);
            return cartRecord;
        } catch(error) {
            throw { error, message: cartErrors.ERROR_FINDING_CART_ITEM_BY_ID.message };
        }
    }

    /**
     * @description This method will help update a Cart record
     * 
     * @param {Object} condition
     */
     async updateRecord(_id, data) {
        try {
            const cartRecord = await this.cartModel.findByIdAndUpdate(_id, { ...data }, {new: true});
            return cartRecord;
        } catch(error) {
            throw { error, message: cartErrors.ERROR_UPDATING_CART_ITEM.message };
        }
    }

    /**
     * @description This method will help delete a Card Item
     * 
     * @param {Object} condition
     */
     async deleteRecordById(_id) {
        try {
            const cartRecord = await this.cartModel.findByIdAndDelete(_id);
            return cartRecord;
        } catch(error) {
            throw { error, message: cartErrors.ERROR_DELETING_CART_ITEM.message };
        }
    }

    /**
     * @description This method will help delete cart after order confirmation
     * 
     * @param {Object} condition
     */
    async deleteAllRecordsInCart(condition) {
        try {
            const cartRecord = await this.cartModel.deleteMany(condition);
            return cartRecord;
        } catch(error) {
            throw { error, message: cartErrors.ERROR_DELETING_CART_ITEMS_AFTER_ORDER_CONFIRMATION.message };
        }
    }
}