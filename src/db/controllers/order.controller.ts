import { Service, Inject } from "typedi";
import Errors from "../../errors/order";

@Service()
export default class OrderController {
    constructor(
        @Inject('orderModel') private orderModel: Models.OrderModel,
        @Inject('logger') private logger,
    ) {}

    /**
     * @description This method will help create a Order record
     * 
     * @param {Object} dataObject
     */
    async createRecord(dataObject) {
        try {
            const orderRecord = await this.orderModel.create(dataObject);
            return orderRecord
        } catch(error) {
            throw { error, message: Errors.ERROR_CREATING_ORDER.mesage };
        }
    }

    /**
     * @description This method will help find an Order record
     * 
     * @param {Object} condition
     */
    async findRecord(condition) {
        try {
            const orderRecord = await this.orderModel.find({condition});
            return orderRecord;
        } catch(error) {
            throw { error, message: Errors.ERROR_FINDING_ORDER.message };
        }
    }

    /**
     * @description This method will help find an Order Record By Id
     * 
     * @param {Object} condition
     */
     async findRecordById(_orderId) {
        try {
            const orderRecord = await this.orderModel.findById(_orderId);
            return orderRecord;
        } catch(error) {
            throw { error, message: Errors.ERROR_FINDING_ORDER_BY_ID.message };
        }
    }

    /**
     * @description This method will help update an Order record
     * 
     * @param {Object} condition
     */
     async updateRecord(_id, data) {
        try {
            const orderRecord = await this.orderModel.findByIdAndUpdate(_id, { ...data }, {new: true});
            return orderRecord;
        } catch(error) {
            throw { error, message: Errors.ERROR_UPDATING_ORDER.message };
        }
    }

    /**
     * @description This method will help delete an Order
     * 
     * @param {Object} condition
     */
     async deleteRecordById(_id) {
        try {
            const orderRecord = await this.orderModel.findByIdAndDelete(_id);
            return orderRecord;
        } catch(error) {
            throw { error, message: Errors.ERROR_DELETING_ORDER.message };
        }
    }
}